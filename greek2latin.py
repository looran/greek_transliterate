#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Converts greek alphabet words (UTF8) to latin alphatbet (ASCII).

# Copyright (c) 2013, Laurent Ghigonis <laurent@gouloum.fr>
# Copyright (c) 2013, Nadege Rollet <ndg.rollet@gmail.com>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

# Reference for coding with utf-8 in python:
# http://www.evanjones.ca/python-utf8.html

import sys
import os
import unittest
import codecs

class Letterization(object):
	def __init__(self):
		path = os.path.abspath(os.path.dirname(sys.argv[0]))
		table = codecs.open(path + '/utf8_ascii_conversion_table.txt', 'r', 'utf-8').read().split('\n')
		self.conv = list()
		for entry in table:
			if entry == u"" or entry.startswith(u"#"): # ignore empty line or comments
				continue
			entry = entry.split(',')
			self.conv.append(entry)
		# conv is now [ ['greek chars', 'corresponding latin char'], ... ]

	def convert_word(self, word):
		word = unicode(word, "utf-8")
		newword = ""
		# each letter of the word
		for i in range(len(word)):
			char = word[i]
			if char in [u"\n", u" "]: # ignore some chars
				continue
			# search in conv table
			newchar = self._conv_search(char)
			if newchar:
				newword += newchar.encode("utf-8")
			else:
				raise Exception("Cannot find char %s" % char.encode("utf-8"))
		return newword

	def _conv_search(self, char):
		for c in self.conv:
			if char in c[0]:
				return c[1]
		return None


class Letterization_unitests(unittest.TestCase):
	def test_onechar(self):
		let = Letterization()
		self.assertEquals(let.convert_word("β"), "b")
		self.assertEquals(let.convert_word("ϐ"), "b")
		self.assertEquals(let.convert_word("έ"), "e")

	def test_word(self):
		expected_res = "bgdzqklmncprstfxy"
		let = Letterization()
		res = let.convert_word("βγδζθκλμνξπρστφχψ")
		self.assertEquals(res, expected_res)

if __name__ == "__main__":
	if "UNITTEST" in os.environ:
		sys.exit(unittest.main())

	let = Letterization()
	for word in sys.stdin:
		print let.convert_word(word)
