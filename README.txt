greek_transliterate - Ancient greek character transliteration tools
Part of the daphnis project
2013, Laurent Ghigonis <laurent@gouloum.fr>
2013, Nadege Rollet <ndg.rollet@gmail.com>

greek2latin.py
Converts greek alphabet words (UTF-8) to latin alphatbet (ASCII).
Accentuation and breathings are removed during transliteration.
Takes a list of *lowercase, one word per line* in stdin, outputs to stdout.

Example usage:

$ cat example/homer_illiad_v1_utf8.txt  | python greek2latin.py 
mhnin
aeide
qea
phlhiadew
axilhos

Original text:
$ cat example/homer_illiad_v1_utf8.txt
μῆνιν
ἄειδε
θεὰ
πηληιάδεω
ἀχιλῆος

